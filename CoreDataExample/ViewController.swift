//
//  ViewController.swift
//  CoreDataExample
//
//  Created by iOS 20 on 18/5/16.
//  Copyright © 2016 iOS 20. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
     @IBOutlet weak var myTable: UITableView!
      var users: Array<Usuario>?
    
    var appDelegate:AppDelegate?
     var context:NSManagedObjectContext?

    
       
    override func viewWillAppear(animated: Bool) {
        
        let request = NSFetchRequest(entityName: "User")
        let results = try? context!.executeFetchRequest(request)
        print(results)
        
        if let results2 = try? context!.executeFetchRequest(request) where results2.count > 0
        {
            for result in results2 {
                
                let user = Usuario(name: result.valueForKey("name") as! String, city: result.valueForKey("city") as! String)
                
                self.users?.append(user)
                
                print(result.valueForKey("name"))
            }
        }

        
        myTable.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.users = []
       appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
        context = appDelegate!.managedObjectContext
        
        // FETCH DATA
      /*
        let myName = "Superman"
        
        if let resultSearch = try? context.executeFetchRequest(request) where resultSearch.count > 0 {
            for result in resultSearch {
                if let userNameSearch = result.valueForKey("name") as? String where userNameSearch == myName {
                    print(result.valueForKey("city"))
                }
            }
        
        } else {
            print("NO HAY DATOS")
        }
        
        // DELETE DATA
        
        if let resultsDelete = try? context.executeFetchRequest(request) where resultsDelete.count > 0 {
        
            for result in resultsDelete {
                if let userNameSearch = result.valueForKey("name") as? String where userNameSearch == myName {
                    print("ES SUPERMAN")
                    context.deleteObject(result as! NSManagedObject)
                }
            }
            _ = try? context.save()
            
        } else {
            print("BORRANDO :::::NO HAY DATOS")
        }
        
        
        //BUSCAR POR PREDICADO
        
        let requestPred = NSFetchRequest(entityName: "User")
        
        requestPred.predicate = NSPredicate(format: "name = %@", "Batman")
        
        if let resultPred = try? context.executeFetchRequest(requestPred) {
            for result in resultPred {
                print(result.valueForKey("city"))
            }
        }
        
        
        let requestMod = NSFetchRequest(entityName: "User")
        
        requestMod.predicate = NSPredicate(format: "name = %@", "Batman")
        
        if let resultMod = try? context.executeFetchRequest(requestMod) {
            for result in resultMod {
                let username = result.valueForKey("name")!
                let city = result.valueForKey("city")!
                
                print("USUARIO:: \(username), CUIDAD :: \(city)")
                result.setValue("BARCELONA", forKey: "city")
                _ = try? context.save()
            }
        }*/

     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dbDeleteUser(user:Usuario) {
        // DELETE DATA
         let request = NSFetchRequest(entityName: "User")
        if let resultsDelete = try? self.context!.executeFetchRequest(request) where resultsDelete.count > 0 {
            
            for result in resultsDelete {
                if let userNameSearch = result.valueForKey("name") as? String where userNameSearch == user.name {
                   // print("ES SUPERMAN")
                    context!.deleteObject(result as! NSManagedObject)
                }
            }
            _ = try? context!.save()
            
        } else {
            print("BORRANDO :::::NO HAY DATOS")
        }

    }
    
    @IBAction func addUser(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("addVC") as! AddViewController
       
        
        self.navigationController?.pushViewController(vc, animated: true)
        
       /* dbAddUserWithName("MOMO", city: "California")
        myTable.reloadData()
        */
    }
    
    


}

extension ViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users!.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("detalleVC") as! DetalleViewController
        vc.user = users![indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellItem")! as UITableViewCell
        
        
        
        cell.textLabel?.text = (users![indexPath.row]).name
        cell.detailTextLabel?.text = (users![indexPath.row]).city
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            dbDeleteUser(users![indexPath.row])
            self.users?.removeAtIndex(indexPath.row)
            myTable.reloadData()
        }
    }


}

