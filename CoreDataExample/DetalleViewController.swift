//
//  DetalleViewController.swift
//  CoreDataExample
//
//  Created by iOS 20 on 19/5/16.
//  Copyright © 2016 iOS 20. All rights reserved.
//

import UIKit

class DetalleViewController: UIViewController {
    
    var user: Usuario?

    @IBOutlet weak var citylbl: UILabel!
    @IBOutlet weak var namelbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        namelbl.text = user?.name
        citylbl.text = user?.city
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func volverAction(sender: UIButton) {
        
        navigationController?.popViewControllerAnimated(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
