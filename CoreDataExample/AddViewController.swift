//
//  AddViewController.swift
//  CoreDataExample
//
//  Created by iOS 20 on 19/5/16.
//  Copyright © 2016 iOS 20. All rights reserved.
//

import UIKit
import CoreData

class AddViewController: UIViewController {

    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dbAddUserWithName(name:String, city:String) {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context:NSManagedObjectContext = appDelegate.managedObjectContext
        
        let newUser = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: context)
        
        newUser.setValue(name, forKey: "name")
        newUser.setValue(city, forKey: "city")
        
        do {
            try context.save()
            
        } catch let error{
            print(error)
        }
        _ = try? context.save()
        
    }

    
    @IBAction func addUsuario(sender: UIButton) {
        
        dbAddUserWithName(nameTxt.text!, city: cityTxt.text!)
        self.navigationController?.popViewControllerAnimated(true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
