//
//  SecondViewController.swift
//  CoreDataExample
//
//  Created by iOS 20 on 19/5/16.
//  Copyright © 2016 iOS 20. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
  
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var myTxtField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loadImage(sender: UIButton) {
        if let documentsDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first {
        
            let imageWeb = myTxtField.text! as String
            let fileName = NSURL(fileURLWithPath: imageWeb)
            
            let savePath = "\(documentsDirectory)/\(fileName)"
            
            if self.myImageView.image == nil {
                print("NO HAY COPIA DE IMÄGEN")
                
                let url = NSURL(string: imageWeb)
                
                let urlRequest = NSURLRequest(URL: url!)
                
                let task = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest) {(data, response, error) -> Void in
                    
                    if error != nil {
                         print("ERROR")
                    } else {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            
                            print("Cargada")
                            
                            let image = UIImage(data: data!)
                            self.myImageView.image = image
                        
                        })
                        
                        NSFileManager.defaultManager().createFileAtPath(savePath, contents: data, attributes: nil)
                        print("Imagen Guardada")
                    }
                }
                task.resume()
            
            } else {
                print("LA Tengoo")
            }
        
        }
        
    }


}
