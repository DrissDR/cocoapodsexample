//
//  FirstViewController.swift
//  CoreDataExample
//
//  Created by iOS 20 on 19/5/16.
//  Copyright © 2016 iOS 20. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var myWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loadUrl(sender: UIButton) {
        
        let url = NSURL(string: "http://www.google.es")
        let urlRequest = NSURLRequest(URL: url!)
        
        myWebView.loadRequest(urlRequest)
        
    }

}
